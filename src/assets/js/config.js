import Firebase from 'firebase'

const config = {
  apiKey: "AIzaSyA9wADlfyau7seo9sql_32tL4MIWIC750I",
  authDomain: "vuebudget.firebaseapp.com",
  databaseURL: "https://vuebudget.firebaseio.com",
  storageBucket: "vuebudget.appspot.com",
  messagingSenderId: "824319692033"
};

const firebase = Firebase.initializeApp(config)
const db = firebase.database()
const provider = new Firebase.auth.GoogleAuthProvider()

firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    localStorage.setItem('userID', user.uid)
    localStorage.setItem('userName', user.displayName)
  }
})

export default {
  db: db,
  firebase: firebase,
  provider: provider
}