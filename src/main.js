import Vue from 'vue'
import VueRouter from 'vue-router'
import VueFire from 'vuefire'
import App from './App'

Vue.use(VueRouter, VueFire)

import routes from './routes'

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  scrollBehavior: () => ({ y: 0 }),
  routes
})

/* eslint-disable no-new */
const app = new Vue({
  router,
  ...App
}).$mount('#app')

