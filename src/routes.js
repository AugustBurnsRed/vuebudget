export default [
	{
    path: '/',
    name: 'home',
    component: { template: '<h1>Accueil</h1>' }
  },
  {
    path: '/budget',
    name: 'budget',
    component: require('./components/Budget/Budget.vue')
  },
  {
    path: '/transaction',
    name: 'transaction',
    component: require('./components/Transaction/Transaction.vue')
  },
  {
    path: '/shortcut',
    name: 'shortcut',
    component: require('./components/pages/Shortcut.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: require('./components/pages/Login.vue')
  },
  {
    path: '/account',
    name: 'account',
    component: { template: '<h1>test</h1>' }
  },
]
